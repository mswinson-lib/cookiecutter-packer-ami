.PHONY: smoke clean

clean:
	@rm -rf ./tmp

smoke:
	@cookiecutter ./ --no-input --output-dir ./tmp --verbose
	
