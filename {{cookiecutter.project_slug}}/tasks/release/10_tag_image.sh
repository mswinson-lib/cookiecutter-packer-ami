for f in $AMI_DIR/release/*.json
do
  filename=`basename $f`
  region="${filename%.*}"
  ami_id=`jq -r '.ImageId' $f`
  echo "waiting for image $ami_id"
  aws ec2 wait image-available --image-ids $ami_id --region $region
  echo "tagging image $ami_id"
  aws ec2 create-tags --resources $ami_id --tags Key=Name,Value=$AMI_NAME --region $region
  aws ec2 create-tags --resources $ami_id --tags Key=Version,Value=$AMI_VERSION --region $region
done
