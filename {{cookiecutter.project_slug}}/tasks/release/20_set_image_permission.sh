case $DEST_AMI_VISIBILITY in
  public)
    LAUNCH_PERMISSION="Add=[{Group=all}]"
    ;;
  private)
    LAUNCH_PERMISSION="Remove=[{Group=all}]"
    ;;
esac

for f in $AMI_DIR/release/*.json
do
  filename=`basename $f`
  region="${filename%.*}"
  ami_id=`jq -r '.ImageId' $f`
  echo "waiting for image $ami_id"
  aws ec2 wait image-available --image-ids $ami_id --region $region
  echo "making image $ami_id $DEST_AMI_VISIBILITY"
  aws ec2 modify-image-attribute \
    --image-id $ami_id \
    --launch-permission $LAUNCH_PERMISSION \
    --region $region
done
