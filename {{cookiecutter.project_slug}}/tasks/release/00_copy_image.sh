for f in $AMI_DIR/build/*.txt
do
  SOURCE_AMI=`cat $f`

  IFS=','
  for region in $DEST_REGION
  do
    aws ec2 copy-image \
      --name "$DEST_AMI_NAME" \
      --source-image-id "$SOURCE_AMI" \
      --source-region "$SOURCE_REGION" \
      --region "$region" \
      > $AMI_DIR/release/$region.json
  done
done
