#!/bin/bash
DIR=$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )
mkdir -p $REPORT_DIR/build
mkdir -p $AMI_DIR/build

for f in $DIR/build/*.sh
do
  chmod u+x $f
  $f
done
