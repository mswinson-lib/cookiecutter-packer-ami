#!/bin/bash
DIR=$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )
mkdir -p $AMI_DIR/release

for f in $DIR/release/*.sh
do
  chmod u+x $f
  $f
done
